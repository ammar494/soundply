//
//  MusicIndustry.swift
//  VideoChatApp
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MusicIndustry: UIViewController {
    
    var loadingIndicator = UIActivityIndicatorView()

    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
    }

    @IBAction func continueBtnClicked(_ sender: Any) {
//        startLoader()
        UserInfo.shared.musicIndustry = txtView.text
        
//        let params : [String : String] = ["" : ""]
//
//        let artistService = ArtistService()
//        artistService.uploadUserData(params: params) { (success, response) in
//            if success == true {
//                DispatchQueue.main.async {
//                    self.stopLoader()
//
//                    let parentVC = self.parent as! PageViewController
//                    parentVC.setViewControllers([Constants.shared.viewControllerArr[10]], direction: .forward, animated: true, completion: nil)
//                }
//            }
//        }
        
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[10]], direction: .forward, animated: true, completion: nil)
    }
    
    func startLoader() {
        self.view.isUserInteractionEnabled = false
        loadingIndicator.frame = CGRect(x: (self.view.frame.width / 2) - 50, y: (self.view.frame.height / 2) - 25, width: 25, height: 50)
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        self.view.addSubview(loadingIndicator)
    }
    
    func stopLoader() {
        loadingIndicator.stopAnimating()
        loadingIndicator.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
}
