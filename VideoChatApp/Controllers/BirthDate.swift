//
//  BirthDate.swift
//  VideoChatApp
//
//  Created by Apple on 01/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BirthDate: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var ageLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
//        datePicker.setValue(false, forKey: "highlightsToday")
        
        
        
    }
    
    func getData() {
        if UserInfo.shared.dob != nil {
            datePicker.date = UserInfo.shared.dob!
            let age = calcAge(birthday: datePicker.date.toString(dateFormat: "MM/dd/yyyy"))
            ageLbl.text = "Age : "+String(age)
        }
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM/dd/yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    @IBAction func continueBtnClicked(_ sender: Any) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[5]], direction: .forward, animated: true, completion: nil)
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        let age = calcAge(birthday: datePicker.date.toString(dateFormat: "MM/dd/yyyy"))
        UserInfo.shared.dob = datePicker.date
        ageLbl.text = "Age : "+String(age)
    }
}
