//
//  ProfileViewController.swift
//  VideoChatApp
//
//  Created by Apple on 19/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

import AVKit
import AVFoundation
import MediaPlayer
import AudioToolbox
import SDWebImage

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var coverPhoto: UIImageView!
    
    @IBOutlet weak var displayNameLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var ethnicityLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var musicLbl: UILabel!
    @IBOutlet weak var stageLbl: UILabel!
    @IBOutlet weak var musicTimeLbl: UILabel!
    
    @IBOutlet weak var profileTbl: UITableView!
    
    let actionSheet = ActionSheet()
    
    var videoArr = [VideoModel]()
    var masterVideoArr = [VideoModel]()
    
    var loadingIndicator = UIActivityIndicatorView()
    var refreshControl = UIRefreshControl()
    
    var isRefeshingTable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTbl.delegate = self
        profileTbl.dataSource = self
        
        displayNameLbl.text = (UserInfo.shared.firstName ?? "")+" "+(UserInfo.shared.lastName ?? "")
        infoLbl.text = "Email : "+(UserInfo.shared.email ?? "")
        genderLbl.text = "Gender : "+(UserInfo.shared.gender ?? "")
        if UserInfo.shared.dob != nil {
            dobLbl.text = "Birth Date : "+convertDateToStr(date: UserInfo.shared.dob!)
        } else {
            dobLbl.text = "Birth Date : "
        }
        ethnicityLbl.text = "Ethnicity : "+(UserInfo.shared.ethnicity ?? "")
        genreLbl.text = "Genre : "+(UserInfo.shared.musicGenre ?? "")
        musicLbl.text = "Location : "+(UserInfo.shared.location ?? "")
        musicTimeLbl.text = "Music Duration : "+(UserInfo.shared.timeInMusic ?? "")
        
        self.navigationItem.title = "Profile"
        //self.tabBarController?.delegate = self
        
        let profileViewTap = UITapGestureRecognizer(target: self, action: #selector(self.profileViewTapped(_:)))
        profileView.addGestureRecognizer(profileViewTap)
        
        guard let coverPic=UserInfo.shared.coverPhotoUrl else{return}
        coverPhoto.contentMode = .scaleAspectFit
        coverPhoto.downloaded(from: coverPic)
        coverPhoto.contentMode = .scaleAspectFit

        profilePic.layer.cornerRadius = 40
        profilePic.layer.borderWidth = 1
        profilePic.layer.masksToBounds = false
        profilePic.layer.borderColor = UIColor.black.cgColor
        profilePic.clipsToBounds = true
//        profilePic.image = UIImage(named: "dummy_black")
        profilePic.downloaded(from: UserInfo.shared.dpUrl!)
        
        profileView.isHidden = false
        
        startLoader()
        getData(genre: "")
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        profileTbl.refreshControl = refreshControl
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
            self.profileTbl.reloadData()
        })
        
    }
    
    func convertDateToStr(date : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    

    @objc func pullToRefresh(sender: UIRefreshControl) {
            isRefeshingTable = true
            getData(genre: "")
    //        sender.endRefreshing()
    //
    //        followingTbl.reloadData()
        }
    
    @objc func profileViewTapped(_ sender: UITapGestureRecognizer) {
        sender.view?.isHidden = true
        
    }
    
    @objc func btnVideoTapped(btn:UIButton)
    {
        let videoURL = URL(string: (videoArr[btn.tag].videoUrl?.replacingOccurrences(of: "\"", with: ""))!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @objc func btnOpenProfileTapped(btn:UIButton)
    {
        
//        displayNameLbl.text = videoArr[btn.tag].artistName
//        infoLbl.text = ""//videoArr[btn.tag].
//        genderLbl.text = videoArr[btn.tag].genre
//        coverPhoto.image=UIImage(named: "")
//        let dp = videoArr[btn.tag].profilePicUrl!.replacingOccurrences(of: "\"", with: "") //else{return}
//        profilePic.downloaded(from: dp)
        
        profileView.isHidden = false
    }
    
    
    @IBAction func menuBtnClicked(_ sender: Any) {
            let buttonPosition = (sender as AnyObject).convert((sender as AnyObject).bounds.origin, to: self.profileTbl)
            
            if let indexPath = profileTbl.indexPathForRow(at: buttonPosition) {
                let rowIndex =  indexPath.row
                print(rowIndex)
                
                
                
                let share = ["Share", "Delete"]
                actionSheet.sheetPopUp(controller: self, options: share) { (response) in
                    print("response : ", response)
                    
                    switch (response) {
                    case "Share":
                        print("Share Pressed")
                        let videoUrl=self.videoArr[rowIndex].videoUrl
                        let items = [videoUrl]
                        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                        self.present(ac, animated: true)
                        
                    case "Delete":
                        print("Delete Pressed")
                        guard let videoID=self.videoArr[rowIndex].ID else{return}
                        self.deleteVideo(videoId: videoID)
                        
                        
                    default:
                        print("Nothing")
                    
                    }
                }
            }
        }
    
    
    func deleteVideo(videoId : String) {
        //        startLoader()
        let videoService = VideoService()
        let param=["videoid":videoId]
        videoService.deleteVideoData(params: param) { (success) in
            if success{
                
                self.getData(genre: "")
                //self.profileTbl.reloadData()
            }
            else{
                alert(msg: "Video not Deleted", controller: self)
            }
        }
    }
    
    
    func getData(genre : String) {
        //        startLoader()
        let videoService = VideoService()
        videoService.getMyVideosData(genre: genre) { (success, response) in
            if success == true {
                self.masterVideoArr = response
                self.videoArr = response
                
                DispatchQueue.main.async {
                    if self.isRefeshingTable == true {
                        self.refreshControl.endRefreshing()
                    }
                    self.profileTbl.reloadData()
                    self.stopLoader()
                }
                    
//                DispatchQueue.main.async {
//                    let artistService = ArtistService()
//                    artistService.getFollowedUsers { (success, followUsers) in
//                        print(followUsers)
//                        if success == true {
//                            for video in 0 ..< self.masterVideoArr.count {
//                                for user in followUsers {
//                                    if self.masterVideoArr[video].loginID! == user {
//                                        self.masterVideoArr[video].isFollowing = true
//                                        self.videoArr = self.masterVideoArr
//
//                                    }
//                                }
//
//                            }
//
////                            artistService.getBlockedUsers(completion: { (success, blockedUsers) in
////                                if success == true {
////                                    for video in 0 ..< self.masterVideoArr.count {
////                                        for user in blockedUsers {
////                                            if self.masterVideoArr[video].loginID! == user {
////                                                self.masterVideoArr[video].isBlocked = true
////                                                self.videoArr = self.masterVideoArr
////                                            }
////                                        }
////
////                                    }
////                                }
////                            })
//
//                            DispatchQueue.main.async {
//                                if self.isRefeshingTable == true {
//                                    self.refreshControl.endRefreshing()
//                                }
//                                self.profileTbl.reloadData()
//                                self.stopLoader()
//
//
//                            }
//
//                        } else {
//                            DispatchQueue.main.async {
//                                self.stopLoader()
//                            }
//                        }
//
//                    }
//
//                }
            } else {
                DispatchQueue.main.async {
                    self.stopLoader()
                }
            }
        }
    }
    
    func startLoader() {
        self.view.isUserInteractionEnabled = false
        loadingIndicator.frame = CGRect(x: (self.view.frame.width / 2) - 50, y: (self.view.frame.height / 2) + 50, width: 25, height: 50)
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        self.view.addSubview(loadingIndicator)
    }
    
    func stopLoader() {
        loadingIndicator.stopAnimating()
        loadingIndicator.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource, AVPlayerViewControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return videoArr.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        
        cell.btnVideo.tag=indexPath.row
        cell.btnVideo.addTarget(self, action: #selector(self.btnVideoTapped(btn:)), for: UIControl.Event.touchUpInside)
        
        cell.btnOpenProfile.tag=indexPath.row
        cell.btnOpenProfile.addTarget(self, action: #selector(self.btnOpenProfileTapped(btn:)), for: UIControl.Event.touchUpInside)
        
        
        print("Cell Called")
        print("isFollowing : ", videoArr[indexPath.row].isFollowing)
        cell.displayPicture.image = UIImage(named: "dummy_black")
        if let dpp=videoArr[indexPath.row].profilePicUrl
        {
            let dp = videoArr[indexPath.row].profilePicUrl!.replacingOccurrences(of: "\"", with: "")
            print("display pic url before : ", videoArr[indexPath.row].profilePicUrl!)
            cell.displayPicture.downloaded(from: dp)
            
//            cell.displayPicture.sd_setImage(with: URL(string: dp), placeholderImage: UIImage(named: "default"))
        }
        
//        cell.displayPicture.sd_setImage(with: URL(string: videoArr[indexPath.row].profilePicUrl!), placeholderImage: UIImage(named: "default"))
    
        //print("dp : ", dp)
//        if let proPic=videoArr[indexPath.row].profilePicUrl
//        {
//            print("display pic url after : ", videoArr[indexPath.row].profilePicUrl!.replacingOccurrences(of: "\"", with: ""))
//
//        }
        cell.displayName.text = videoArr[indexPath.row].artistName
        cell.title.text = videoArr[indexPath.row].caption
        var videoUrl = videoArr[indexPath.row].videoUrl?.replacingOccurrences(of: "\"", with: "")
        
        if videoUrl == nil {
            videoUrl = "http://www.waqarulhaq.com/onboard-app/videos/sample.mp4"
        }
        
        if let videoUrl1 = URL(string: videoUrl!){
            print("cell : ", indexPath.row)
            print(videoUrl1)
            let player = AVPlayer(url: videoUrl1)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = cell.videoView.bounds
            cell.videoView.layer.addSublayer(playerLayer)
            player.volume=0
            player.play()
            
//                avPlayerArr.append(player)
            
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        }
        
        return cell
            
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellHeight: CGFloat = 20.0
        if tableView == profileTbl {
            cellHeight = 380
        }
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelect")
        
    }

    func playerViewControllerWillStartPictureInPicture(_ playerViewController: AVPlayerViewController){
        print("playerViewControllerWillStartPictureInPicture")
    }
    
    func playerViewControllerDidStartPictureInPicture(_ playerViewController: AVPlayerViewController)
    {
        print("playerViewControllerDidStartPictureInPicture")
        
    }
    func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error)
    {
        print("failedToStartPictureInPictureWithError")
    }
    func playerViewControllerWillStopPictureInPicture(_ playerViewController: AVPlayerViewController)
    {
        print("playerViewControllerWillStopPictureInPicture")
    }
    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController)
    {
        print("playerViewControllerDidStopPictureInPicture")
    }
    func playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart(_ playerViewController: AVPlayerViewController) -> Bool
    {
        print("playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart")
        return true
    }
    

}
