//
//  Ethnicity.swift
//  VideoChatApp
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Ethnicity: UIViewController {

    @IBOutlet var btnCollection: [UIButton]!
    
    var btnUnselectedBgColor : UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnUnselectedBgColor = btnCollection[0].backgroundColor
        getData()
    }
    
    func getData() {
        if UserInfo.shared.ethnicity != nil {
            selectedButton(title: UserInfo.shared.ethnicity!)
        }
    }
    
    @IBAction func optionClicked(_ sender: UIButton) {
        selectedButton(title: sender.title(for: .normal)!)
    }
    
    func selectedButton(title : String) {
        for btn in btnCollection {
            if btn.title(for: .normal) == title {
                UserInfo.shared.ethnicity = title
                btn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
            } else {
                btn.backgroundColor = btnUnselectedBgColor
            }
        }
    }
    
    @IBAction func continueBtnClicked(_ sender: Any) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[7]], direction: .forward, animated: true, completion: nil)
    }
    

}
