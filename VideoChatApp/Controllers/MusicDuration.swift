//
//  MusicDuration.swift
//  VideoChatApp
//
//  Created by Apple on 29/04/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MusicDuration: UIViewController {

    @IBOutlet weak var pickerView: UIPickerView!
    
    var yearsArr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
    }

    @IBAction func continueBtnClicked(_ sender: Any) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[9]], direction: .forward, animated: true, completion: nil)
    }
}

extension MusicDuration : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearsArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Component : ", component)
        return yearsArr[row] + "    Years"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(yearsArr[row])
        UserInfo.shared.timeInMusic = String(yearsArr[row])
    }
    
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        return NSAttributedString(string: yearsArr[row] + "    Years" , attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
//    }
    
    
}
