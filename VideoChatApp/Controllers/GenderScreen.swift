//
//  GenderScreen.swift
//  VideoChatApp
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GenderScreen: UIViewController {

    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var groupBtn: UIButton!
    
    var btnUnSelectBgColor: UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnUnSelectBgColor = maleBtn.backgroundColor
        
        getData()
    }
    
    func getData() {
        if UserInfo.shared.gender != nil {
            if UserInfo.shared.gender == "Male" {
                maleBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
            } else if UserInfo.shared.gender == "Female" {
                femaleBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
            }else {
                groupBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
            }
        }
    }

    @IBAction func continueBtnClicked(_ sender: Any) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[4]], direction: .forward, animated: true, completion: nil)
    }
    
    @IBAction func maleBtnClicked(_ sender: UIButton) {
        maleBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
        femaleBtn.backgroundColor = btnUnSelectBgColor
        groupBtn.backgroundColor = btnUnSelectBgColor
        UserInfo.shared.gender = "Male"
    }

    @IBAction func femaleBtnClicked(_ sender: UIButton) {
        femaleBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
        maleBtn.backgroundColor = btnUnSelectBgColor
        groupBtn.backgroundColor = btnUnSelectBgColor
        
        UserInfo.shared.gender = "Female"
    }

    @IBAction func groupBtnClicked(_ sender: UIButton) {
        groupBtn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
        femaleBtn.backgroundColor = btnUnSelectBgColor
        maleBtn.backgroundColor = btnUnSelectBgColor
        
        UserInfo.shared.gender = "Group"
    }
}
