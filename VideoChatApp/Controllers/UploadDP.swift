//
//  UploadDP.swift
//  VideoChatApp
//
//  Created by Apple on 12/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class UploadDP: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var loadingIndicator = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func uploadBtnAction(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController ()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
//        imagePickerController.mediaTypes = ["public.movie"]
        present(imagePickerController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            startLoader()
            let artistService = ArtistService()
            artistService.uploadImageAlamo(image: pickedImage) { (success, response) in
                print(response)
                
                if success == true {
                    UserInfo.shared.dpUrl = response
                    DispatchQueue.main.async {
                        self.stopLoader()
                        
//                        alertWithCompletion(msg: "Display Picture successfully uploaded!", controller: self, onCompletion: { (success) in
                            let parentVC = self.parent as! PageViewController
                            parentVC.setViewControllers([Constants.shared.viewControllerArr[11]], direction: .forward, animated: true, completion: nil)
//                            return
//                        })
                    }
                    
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func startLoader() {
        self.view.isUserInteractionEnabled = false
        loadingIndicator.frame = CGRect(x: (self.view.frame.width / 2) - 50, y: (self.view.frame.height / 2) - 25, width: 25, height: 50)
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        self.view.addSubview(loadingIndicator)
    }
    
    func stopLoader() {
        loadingIndicator.stopAnimating()
        loadingIndicator.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
}
