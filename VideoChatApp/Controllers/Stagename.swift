//
//  Stagename.swift
//  VideoChatApp
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Stagename: UIViewController {

    @IBOutlet weak var stageTxtFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        getData()
    }
    
    func setData() {
        if stageTxtFiled.text != "" {
            UserInfo.shared.stageName = stageTxtFiled.text
        }
        
    }
    
    func getData() {
        
        if UserInfo.shared.stageName != nil {
            stageTxtFiled.text = UserInfo.shared.stageName
        }
    }

    @IBAction func continueBtnClicked(_ sender: Any) {
        UserInfo.shared.stageName = stageTxtFiled.text
        
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[3]], direction: .forward, animated: true, completion: nil)
    }
}
