//
//  MusicGenre.swift
//  VideoChatApp
//
//  Created by Apple on 04/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MusicGenre: UIViewController {

    @IBOutlet var genreBtns: [UIButton]!
    
    var btnUnselectedBgColor : UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnUnselectedBgColor = genreBtns[0].backgroundColor
        getData()
    }
    
    func getData() {
        if UserInfo.shared.musicGenre != nil {
            selectedButton(title: UserInfo.shared.musicGenre!)
        }
    }
    
    @IBAction func genreBtnClicked(_ sender: UIButton) {
        selectedButton(title: sender.title(for: .normal)!)
    }
    
    func selectedButton(title : String) {
        for btn in genreBtns {
            if btn.title(for: .normal) == title {
                UserInfo.shared.musicGenre = title
                btn.backgroundColor = UIColor(rgb: Constants.shared.appThemeColor)
            } else {
                btn.backgroundColor = btnUnselectedBgColor
            }
        }
    }
    
    @IBAction func continueBtnClicked(_ sender: Any) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([Constants.shared.viewControllerArr[8]], direction: .forward, animated: true, completion: nil)
    }
    
}
