//
//  Alerts.swift
//  VideoChatApp
//
//  Created by Apple on 08/05/2020.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

func alert(msg : String, controller: UIViewController) {
    let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
    controller.present(alert, animated: true, completion: nil)
}

func alertWithCompletion(msg : String, controller: UIViewController, onCompletion:@escaping (Bool) -> Void) {
    let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (success) in
        onCompletion(true)
    }))
    controller.present(alert, animated: true, completion: nil)
}
